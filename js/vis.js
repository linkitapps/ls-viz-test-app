var graph3dChartOptions = {
    type: 'graph3d',
    container: document.getElementById('graph3d-chart')
};

// Create and populate a data table.
var data = lsViz.util.visDataSet();
// create some nice looking data with sin/cos
var counter = 0;
var steps = 50;  // number of datapoints will be steps*steps
var axisMax = 314;
var axisStep = axisMax / steps;
for (var x = 0; x < axisMax; x+=axisStep) {
    for (var y = 0; y < axisMax; y+=axisStep) {
        var value = (Math.sin(x/50) * Math.cos(y/50) * 50 + 50);
        data.add({id:counter++,x:x,y:y,z:value,style:value});
    }
}

// specify options
var options = {
    data: data,
    width:  '600px',
    height: '450px',
    style: 'surface',
    showPerspective: true,
    showGrid: true,
    showShadow: false,
    keepAspectRatio: true,
    verticalRatio: 0.5
};

// vis.js 로 3D 그래프 그리기
lsViz.chart(graph3dChartOptions, options);